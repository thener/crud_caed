package br.com.caed.loja.relatorio;

import java.util.HashMap;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class GeradorRelatorio {

	private String nome;

	private HashMap<String, Object> parametros;
	
	private JRBeanCollectionDataSource dataSource;

	public GeradorRelatorio(String nome, HashMap<String, Object> parametros, JRBeanCollectionDataSource dataSource) {
		this.nome = nome;
		this.parametros=parametros;
		this.dataSource = dataSource;

	}

	public void gerar() throws Exception {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		facesContext.responseComplete();

		ServletContext scontext = (ServletContext) facesContext.getExternalContext().getContext();

		JasperPrint print = JasperFillManager.fillReport(scontext.getRealPath("/WEB-INF/jasper/" + nome + ".jasper"),
				parametros, dataSource);

		JasperExportManager.exportReportToPdfFile(print, "c:/relatorios/Relatorio_de_" + nome + ".pdf");
	}

}
