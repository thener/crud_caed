package br.com.caed.loja.dao;

import java.util.List;

import br.com.caed.loja.modelo.Autor;

public class AutorDAO extends GenericoDAO<Autor>{

	public List<Autor> listar() {
		return entityManager.createQuery("select a from Autor a", Autor.class).getResultList();
	}
}
