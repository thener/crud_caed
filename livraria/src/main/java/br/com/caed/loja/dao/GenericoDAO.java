package br.com.caed.loja.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.caed.loja.modelo.EntidadeBase;

public class GenericoDAO<T extends EntidadeBase> {
	
	@PersistenceContext
	protected EntityManager entityManager;

	/**
	 * Salva ou atualiza um livro, caso já tenha sido salvo anteriormente.
	 * 
	 * @param livro
	 */
	public void salvar(T t) {
		if (t.getId() == null) {
			entityManager.persist(t);
		} else {
			entityManager.merge(t);
		}
	}

	public void remover(T t) {

		entityManager.remove(entityManager.contains(t) ? t : entityManager.merge(t));

	}

	public T buscarPorId(Class<T> clazz, Integer id) {
		return entityManager.find(clazz, id);
	}
}
