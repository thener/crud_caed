package br.com.caed.loja.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.caed.loja.dao.AutorDAO;
import br.com.caed.loja.modelo.Autor;

public class AutorService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private AutorDAO autorDao;

	public List<Autor> listar() {
		return autorDao.listar();
	}

}
