package br.com.caed.loja.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import br.com.caed.loja.modelo.Autor;
import br.com.caed.loja.modelo.Livro;
import br.com.caed.loja.relatorio.GeradorRelatorio;
import br.com.caed.loja.relatorio.RelatorioVO;
import br.com.caed.loja.service.AutorService;
import br.com.caed.loja.service.LivroService;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Named
@SessionScoped
public class LivrosBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private LivroService livroService;

	@Inject
	private AutorService autorService;

	private Livro livro = new Livro();

	private List<Integer> autoresId = new ArrayList<>();

	public List<Livro> getLivros() {
		return livroService.buscarTodos();
	}

	@Transactional
	public String salvar() {
		livroService.salvar(livro, autoresId);
		System.out.println("Livro cadastrado com sucesso " + livro);
		limparFormulario();
		return "/livros/listaLivros?faces-redirect=true";
	}

	@Transactional
	public void remover(Livro livro) {
		livroService.remover(livro);
	}

	public void carregar(Livro livro) {
		setLivro(livro);
	}

	@Transactional
	public void atualizar() {
		livroService.salvar(this.livro, autoresId);
		limparFormulario();
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public List<Autor> getAutores() {
		return autorService.listar();
	}

	public List<Integer> getAutoresId() {
		return autoresId;
	}

	public void setAutoresId(List<Integer> autoresId) {
		this.autoresId = autoresId;
	}

	private void limparFormulario() {
		this.livro = new Livro();
		this.autoresId = new ArrayList<>();
	}

	public void gerarRelatorio() {
		List<Livro> livros = getLivros();
		HashMap<String, Object> parametros = prepararParametros(livros);
		GeradorRelatorio relatorio = new GeradorRelatorio("livros", parametros, new JRBeanCollectionDataSource(livros));
		try {
			relatorio.gerar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private HashMap<String, Object> prepararParametros(List<Livro> livros) {
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		RelatorioVO relatorioVO = new RelatorioVO();
		relatorioVO.setLivros(livros);
		parametros.put("relatorioVO", relatorioVO);
		return parametros;
	}
}
