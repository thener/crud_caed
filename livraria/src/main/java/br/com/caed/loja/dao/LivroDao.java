package br.com.caed.loja.dao;

import java.util.List;

import br.com.caed.loja.modelo.Livro;

public class LivroDao extends GenericoDAO<Livro>{

	public List<Livro> buscarTodos() {

		String jpql = "select distinct(l) from Livro l join fetch l.autores";

		return entityManager.createQuery(jpql, Livro.class).getResultList();
	}

}
