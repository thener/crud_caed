package br.com.caed.loja.relatorio;

import java.util.ArrayList;
import java.util.List;

import br.com.caed.loja.modelo.Livro;

public class RelatorioVO {
	private List<Livro> livros = new ArrayList<>();

	public List<Livro> getLivros() {
		return livros;
	}

	public void setLivros(List<Livro> livros) {
		this.livros = livros;
	}
}
