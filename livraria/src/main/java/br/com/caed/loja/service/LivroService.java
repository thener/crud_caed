package br.com.caed.loja.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.caed.loja.dao.LivroDao;
import br.com.caed.loja.modelo.Autor;
import br.com.caed.loja.modelo.Livro;

public class LivroService implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private LivroDao livroDao;

	public void salvar(Livro livro, List<Integer> autoresId) {
		autoresId.forEach(a -> livro.getAutores().add(new Autor(a)));
		livroDao.salvar(livro);
	}

	public void remover(Livro livro) {
		livroDao.remover(livro);
	}

	public Livro buscarPorId(Integer id) {
		return livroDao.buscarPorId(Livro.class, id);
	}
	
	public List<Livro> buscarTodos() {
		return livroDao.buscarTodos();
	}

}
